<?php

namespace Ata\Cycle\Versioning\Models\Traits;

use Cycle\Annotated\Annotation\Relation\Morphed\MorphedHasMany;
use Ata\Cycle\Versioning\Models\Version;

trait Versionable
{

    /** @MorphedHasMany(target = Version::class) */
    public $versions;

    /**
     * @return static
     */
    public function revertToVersion(int $versionId)
    {
        Version::firstOrFail($versionId)->revert();

        return $this;
    }

    public function createVersion(array $attributes = [])
    {
        Version::create(['content' => $attributes, 'versionable' => $this]);

        return $this;
    }
}
