<?php

namespace Ata\Cycle\Versioning\Models\Interfaces;
use Ata\Cycle\ORM\Models\Interfaces\CycleModelInterface;
use Cycle\Annotated\Annotation\Entity;


/**
 * @Entity
*/
interface VersionableInterface extends CycleModelInterface
{
    /**
     * @return static
    */
    function revertToVersion(int $versionId);

    /**
     * @return static
     */
    function createVersion(array $attributes=[]);
}
