<?php

namespace Ata\Cycle\Versioning\Models;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Ata\Cycle\Versioning\Models\Interfaces\VersionableInterface;
use Cycle\Annotated\Annotation\Column;
use Ata\Cycle\ORM\Typecasts\Json;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\Morphed\BelongsToMorphed;
use Cycle\ORM\Schema;

/**
 * @Entity
*/
class Version extends CycleModel
{
    use IntPrimary;
    use Timestamps;

    /**
     * @Column(type="json", typecast=Json::class)
    */
    public $content;

    /**
     * @BelongsToMorphed(target=VersionableInterface::class)
     *
     * @var VersionableInterface
    */
    public $versionable;

    public $versions_id;

    public $versions_role;

    public function revert()
    {
        $versionableClass = resolve('cycle-db')->getSchema()->define($this->versions_role, Schema::ENTITY);

        $versionableClass::firstOrFail($this->versions_id)->update($this->content->toArray());
    }
}
