<?php


namespace Ata\Cycle\Versioning\MapperCommands\Create;

use Ata\Cycle\ORM\Mappers\Commands\Interfaces\CreateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\Versioning\Models\Interfaces\VersionableInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;
use Ata\Cycle\Versioning\Models\Version as VersionModel;

class Version implements CreateMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        if ($entity instanceof VersionableInterface){
            $entity->versions->add(VersionModel::make(['content' => $state->getData()]));
        }

        return $previousCommand;
    }
}
