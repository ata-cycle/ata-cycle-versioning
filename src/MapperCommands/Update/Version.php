<?php

namespace Ata\Cycle\Versioning\MapperCommands\Update;

use Ata\Cycle\ORM\Mappers\Commands\Interfaces\UpdateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\Versioning\MapperCommands\Traits\CompareTrait;
use Ata\Cycle\Versioning\Models\Interfaces\VersionableInterface;
use Ata\Cycle\Versioning\Models\Version as VersionModel;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Version implements UpdateMapperCommand
{
    use CompareTrait;

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        if ($entity instanceof VersionableInterface){
            $data = $mapper->fetchEntityFields($entity);

            $attributes = array_udiff_assoc($data, $state->getData(), [static::class, 'compare']);

            $entity->versions->add(VersionModel::make(['content' => $attributes]));
        }

        return $previousCommand;
    }
}
