<?php

namespace Ata\Cycle\Versioning\MapperCommands\Traits;

trait CompareTrait
{
    public static function compare($a, $b): int
    {
        if ($a == $b) {
            return 0;
        }

        return ($a > $b) ? 1 : -1;
    }
}
