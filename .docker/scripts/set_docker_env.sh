#!/usr/bin/env sh

touch .env

set -o allexport
[[ -f .env ]] && source .env
set +o allexport

#grep PS1= .env && true || printf '\nPS1=\\e[1;36m\\]\\u@docker\\e[0;37m\\]:\\e[1;34m\\]\\w\\e[0;37m\\]$ \\e[0;37m\\]\n\n' >> .env

grep HOST_UNAME= .env && true || printf "\nHOST_UNAME=%s\n\n" "${USER}" >> .env
grep HOST_UID= .env && true || printf "HOST_UID=%s\n" "$(id -u)" >> .env
grep HOST_GID= .env && true || printf "HOST_GID=%s\n" "$(id -g)" >> .env

grep XDEBUG_CONFIG= .env && true || case "$(uname -s)" in
    Darwin*|CYGWIN*|MINGW*) printf "\nXDEBUG_CONFIG=\"remote_host=host.docker.internal remote_port=9001\"\n" >> .env;;
    *)                      printf "\nXDEBUG_CONFIG=\"remote_host=192.168.220.1 remote_port=9001\"\n" >> .env
esac
grep PHP_IDE_CONFIG= .env && true || printf "PHP_IDE_CONFIG=serverName=AtaCycleOrm\n" >> .env
