<?php

namespace Ata\Cycle\Versioning\Tests\Unit;


use Ata\Cycle\Versioning\Models\Version;
use Ata\Cycle\Versioning\Tests\BaseTestCase;
use Ata\Cycle\Versioning\Tests\Models\TestModel;
use Spiral\Database\Injection\Fragment;

class VersionTest extends BaseTestCase
{

    public function testShouldCreateVersionWhenCreateEntity()
    {
        TestModel::create(['integer_field' => 1]);

        resolve('cycle-db.heap-clean');

        $versions = Version::findAll();

        self::assertNotEmpty($versions);
        self::assertEquals(1, $versions->count());
    }


    public function testShouldCreateVersionWhenUpdateEntity()
    {
        $model = TestModel::create(['integer_field' => 1]);

        $model->update(['integer_field' => 2]);

        resolve('cycle-db.heap-clean');

        $versions = Version::findAll();

        self::assertNotEmpty($versions);
        self::assertEquals(2, $versions->count());
    }

    public function testShouldRestoreVersion()
    {
        $model = TestModel::create(['integer_field' => 1]);

        $model->update(['integer_field' => 2]);

        resolve('cycle-db.heap-clean');

        $version = Version::orderBy('created_at')->findOne();

        $version->revert();

        resolve('cycle-db.heap-clean');

        $model = TestModel::findOne();

        self::assertEquals(1, $model->integer_field);
    }

    public function testShouldQueryOnVersions()
    {
        $model = TestModel::create(['integer_field' => 1]);
        $model->update(['integer_field' => 2]);
        $model->update(['integer_field' => 3]);

        TestModel::create(['integer_field' => 1]);

        $modelCount = TestModel::with('versions')
            ->where(new Fragment("CAST(\"testModel_versions\".content->>'integer_field' AS INTEGER)"), 1)
            ->count();

        self::assertEquals(2, $modelCount);
    }
}
