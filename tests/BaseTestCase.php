<?php

namespace Ata\Cycle\Versioning\Tests;

use Ata\Cycle\ORM\Mappers\Commands\Create\Create;
use Ata\Cycle\ORM\Mappers\Commands\Update\Update;
use Ata\Cycle\Versioning\MapperCommands\Create\Version as CreateVersion;
use Ata\Cycle\Versioning\MapperCommands\Update\Version as UpdateVersion;
use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\Versioning\Tests\Models\TestModel;

abstract class BaseTestCase extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $app['config']->set('cycle.schema.path', ['/app/tests/Models', '/app/src/Models']);

        $app['config']->set('cycle.commands.create', [
            new Create(),
            new \Ata\Cycle\ORM\Mappers\Commands\Create\Timestamps(),
            new CreateVersion(),
        ]);

        $app['config']->set('cycle.commands.update', [
            new UpdateVersion(),
            new Update(),
            new \Ata\Cycle\ORM\Mappers\Commands\Update\Timestamps(),
        ]);
    }
}
