<?php

namespace Ata\Cycle\Versioning\Tests\Models;

use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Ata\Cycle\Versioning\Models\Interfaces\VersionableInterface;
use Ata\Cycle\Versioning\Models\Traits\Versionable;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Ata\Cycle\ORM\Models\CycleModel;

/**
 * @Entity
 *
*/
class TestModel extends CycleModel implements VersionableInterface
{
    use IntPrimary;
    use Timestamps;
    use Versionable;

    /** @Column(type="integer", nullable=true) */
    public $integer_field;
}
